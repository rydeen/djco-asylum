using UnityEngine;
using System.Collections;
using UnityEngine.UI ;

public class MenuVideoPlayer : MonoBehaviour {

	public GUISkin guiSkin ;
	public MovieTexture movie ;
	private int rectWidth = 300 ;
	private int rectHeight = 40 ;

	private float alphaValue ;

	private bool fadeIn ;
	private bool movieEnd ;

	private float movieTime ;

	private bool StopScrictp ;

	void Start () 
	{
		GetComponent<RawImage> ().texture = this.movie as MovieTexture;
		this.movie.Play ();
		this.movieTime = this.movie.duration;
		this.alphaValue = 0.99f;

		this.fadeIn = true;
		this.movieEnd = false;

		this.StopScrictp = false;
	}
	
	void Update () 
	{
		if (this.StopScrictp == false) 
		{
			this.movieTime -= Time.deltaTime;

			if (Input.GetKeyDown (KeyCode.Return)) 
			{
				//Application.LoadLevel("GameLevel") ;
				this.movieEnd = false;
				this.movie.Stop ();
				Camera.main.SendMessage ("fullFadeOut");
				this.gameObject.SendMessage ("LoadScene");
				this.StopScrictp = true ;
			}

			if (this.movieTime < 1) 
			{
				this.movie.Pause ();
				this.movieEnd = true;
			} 
			else if (this.movieTime < 5) 
			{
				this.movieEnd = true;
			}
		}
	}


	void OnGUI()
	{
		if (this.StopScrictp == false) 
		{
			GUI.skin = this.guiSkin;

			if (this.movieEnd) 
			{
				if (!this.fadeIn && this.alphaValue < 1f && this.alphaValue > 0f) 
				{
					GUI.color = new Color (255, 255, 255, this.alphaValue);
					this.alphaValue -= 0.5f * Time.deltaTime;
					GUI.Label (new Rect (Screen.width / 2 - (this.rectWidth / 2), (Screen.height / 4) * 3, this.rectWidth, this.rectHeight), 
				          "PRESS ENTER TO PLAY");

					if (this.alphaValue <= 0f) 
					{
						this.alphaValue = 0.1f;
						this.fadeIn = true;
					}
				} 
				else if (this.fadeIn && this.alphaValue < 1f && this.alphaValue > 0f) 
				{
					GUI.color = new Color (255, 255, 255, this.alphaValue);
					this.alphaValue += 0.5f * Time.deltaTime;
					GUI.Label (new Rect (Screen.width / 2 - (this.rectWidth / 2), (Screen.height / 4) * 3, this.rectWidth, this.rectHeight), 
				          "PRESS ENTER TO PLAY");
			
					if (this.alphaValue > 1f) 
					{
						this.alphaValue = 0.9f;
						this.fadeIn = false;

					}
				}
			}
		}
	}


}
