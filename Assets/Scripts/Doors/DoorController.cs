﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {

	public GUISkin guiSkin ;
	private int rectWidth = 300 ;
	private int rectHeight = 40 ;

	public bool specialDoor = false;

	public Animation doorAnimation ;

	public string color ;

	public bool isOpen ;
	private bool actionEnabled ;

	public bool initialStateOpen ;


	/**********************************************************************/
	/* *****************************  START ***************************** */

	void Start() 
	{
		this.doorAnimation = this.gameObject.GetComponent<Animation> ();
		this.isOpen = false;
		this.actionEnabled = false;

		this.color = "#c8c8c8";

		if (this.initialStateOpen)
			this.isOpen = true;

		if ( this.initialStateOpen )
			this.doorAnimation.CrossFade("Abrir") ;
		else
			this.doorAnimation.CrossFade("Fechar") ;
	}


	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */

	void Update()
	{
		if (this.actionEnabled)
		{
			if ( Input.GetKey ( KeyCode.E ) )
			{
				if (specialDoor) {
					Time.timeScale = 0f;
					this.GetComponent<ChoosePassword>().enabled = true;
				}
				else if ( this.isOpen )
				{
					// close the door 
					this.doorAnimation.CrossFade("Fechar") ;
					this.isOpen = false ;
					this.actionEnabled = false ;
				}
				else
				{
					// open the door
					this.doorAnimation.CrossFade("Abrir") ;
					this.isOpen = true ;
					this.actionEnabled = false ;
				}
			}
		}
	}


	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */
	
	void OnGUI() 
	{
		if ( this.actionEnabled )
		{
			GUI.skin = this.guiSkin;

			if ( ! this.isOpen )
			{
				GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), (Screen.height/4)*3, this.rectWidth , this.rectHeight ), 
				           "PRESS <color="+this.color+">[E]</color> TO OPEN");
			}
			else if ( this.isOpen )
			{
				GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), (Screen.height/4)*3, this.rectWidth , this.rectHeight ), 
				           "PRESS <color="+this.color+">[E]</color> TO CLOSE");
			}
		}
	}

	void OnTriggerEnter( Collider collider )
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = true;
		}
	}

	void OnTriggerExit( Collider collider )
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = false;
		}
	}
}
