﻿using UnityEngine;
using System.Collections;

public class DoorChangeScene : MonoBehaviour {

	public GUISkin guiSkin ;
	private int rectWidth = 300 ;
	private int rectHeight = 40 ;

	public string color ;
	
	private bool actionEnabled ;

	
	/**********************************************************************/
	/* *****************************  START ***************************** */
	
	void Start() 
	{
		this.actionEnabled = false;
		
		this.color = "#c8c8c8";
	}
	
	
	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */
	
	void Update()
	{
		if (this.actionEnabled)
		{
			if ( Input.GetKey ( KeyCode.E ) )
			{
				this.actionEnabled = false ;
				Camera.main.SendMessage ("fullFadeOut");
				GameObject.Find("RawImage").SendMessage("LoadScene") ;
			}
		}
	}
	
	
	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */
	
	void OnGUI() 
	{
		if ( this.actionEnabled )
		{
			GUI.skin = this.guiSkin;

			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), (Screen.height/4)*3, this.rectWidth , this.rectHeight ), 
				          "<color="+this.color+">[E]</color> GO UPSTAIRS");
		}
	}
	
	void OnTriggerEnter( Collider collider )
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = true;
		}
	}
	
	void OnTriggerExit( Collider collider )
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = false;
		}
	}
}
