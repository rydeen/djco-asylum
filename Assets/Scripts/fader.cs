﻿using UnityEngine;
using System.Collections;

public class fader : MonoBehaviour {

	// FadeInOut
	//
	//--------------------------------------------------------------------
	//                        Public parameters
	//--------------------------------------------------------------------
	
	public Texture2D fadeOutTexture;
	public float fadeSpeed = (float)0.2;	
	public int drawDepth = -1000;
	private float alpha = (float)1.0; 	
	private int fadeDir = -1;
	private Color guicolor;
	private bool gameOver = false;
	private bool fadeLowLife = false;

	
	//--------------------------------------------------------------------
	//                       Runtime functions
	//--------------------------------------------------------------------
	
	//--------------------------------------------------------------------
	void Start () {
		alpha=1.0f;
		fadeIn();
		guicolor = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
		if (gameOver && alpha == 1) {
			Destroy( GameObject.Find("NEW_Character") );
			Application.LoadLevel("Menu");
		}
	}


	void OnGUI(){
		
		alpha += fadeDir * fadeSpeed * Time.deltaTime;

		if (fadeLowLife && !gameOver) {
			if (alpha > 0.5f)
				alpha = 0.5f;
		}

		alpha = Mathf.Clamp01(alpha);	

		guicolor.a = alpha;
		GUI.color = guicolor;
		
		GUI.depth = drawDepth;
		
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
	}
	
	//--------------------------------------------------------------------
	
	void fadeIn(){
		fadeDir = -1;	
		fadeLowLife = false;
	}
	
	//--------------------------------------------------------------------
	void smallFadeOut()
	{
		fadeDir = 1;
		fadeLowLife = true;

	}

	void fullFadeOut(){
		fadeDir = 1;
		gameOver = true;
		fadeLowLife = false;

	}

	// Use this for initialization

}
