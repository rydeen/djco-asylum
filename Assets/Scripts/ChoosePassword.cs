﻿using UnityEngine;
using System.Collections;

public class ChoosePassword : MonoBehaviour {

	Texture[] simbolo1 = new Texture[4];
	Texture[] simbolo2 = new Texture[4];
	Texture[] simbolo3 = new Texture[4];
	Texture[] simbolo4 = new Texture[4];
	Texture[] simbolo5 = new Texture[4];
	Texture[] simbolo6 = new Texture[4];
	Texture[] simbolo7 = new Texture[4];
	Texture[] simbolo8 = new Texture[4];
	Texture[] simbolo9 = new Texture[4];
	Texture ok = new Texture();
	Texture okSeleccionado = new Texture();
	bool[] selecao = new bool[9];
	int numSelecionados;
	int sizeIcon;
	int indicadorX;
	int indicadorY;

	// Use this for initialization
	void Start ()
	{

		sizeIcon = 200/3;

		indicadorX = 2;
		indicadorY = 2;

		for (int i = 0; i < 9; i++)
		{
			selecao[i] = false;
		}
		
		simbolo1[0] = (Texture) Resources.Load("Icons/1/semfundo");
		simbolo1[1] = (Texture) Resources.Load("Icons/1/borda");
		simbolo1[2] = (Texture) Resources.Load("Icons/1/fundo");
		simbolo1[3] = (Texture) Resources.Load("Icons/1/fundoeborda");

		simbolo2[0] = (Texture) Resources.Load("Icons/2/semfundo");
		simbolo2[1] = (Texture) Resources.Load("Icons/2/borda");
		simbolo2[2] = (Texture) Resources.Load("Icons/2/fundo");
		simbolo2[3] = (Texture) Resources.Load("Icons/2/fundoeborda");

		simbolo3[0] = (Texture) Resources.Load("Icons/3/semfundo");
		simbolo3[1] = (Texture) Resources.Load("Icons/3/borda");
		simbolo3[2] = (Texture) Resources.Load("Icons/3/fundo");
		simbolo3[3] = (Texture) Resources.Load("Icons/3/fundoeborda");

		simbolo4[0] = (Texture) Resources.Load("Icons/4/semfundo");
		simbolo4[1] = (Texture) Resources.Load("Icons/4/borda");
		simbolo4[2] = (Texture) Resources.Load("Icons/4/fundo");
		simbolo4[3] = (Texture) Resources.Load("Icons/4/fundoeborda");

		simbolo5[0] = (Texture) Resources.Load("Icons/5/semfundo");
		simbolo5[1] = (Texture) Resources.Load("Icons/5/borda");
		simbolo5[2] = (Texture) Resources.Load("Icons/5/fundo");
		simbolo5[3] = (Texture) Resources.Load("Icons/5/fundoeborda");
		
		simbolo6[0] = (Texture) Resources.Load("Icons/6/semfundo");
		simbolo6[1] = (Texture) Resources.Load("Icons/6/borda");
		simbolo6[2] = (Texture) Resources.Load("Icons/6/fundo");
		simbolo6[3] = (Texture) Resources.Load("Icons/6/fundoeborda");
		
		simbolo7[0] = (Texture) Resources.Load("Icons/7/semfundo");
		simbolo7[1] = (Texture) Resources.Load("Icons/7/borda");
		simbolo7[2] = (Texture) Resources.Load("Icons/7/fundo");
		simbolo7[3] = (Texture) Resources.Load("Icons/7/fundoeborda");
		
		simbolo8[0] = (Texture) Resources.Load("Icons/8/semfundo");
		simbolo8[1] = (Texture) Resources.Load("Icons/8/borda");
		simbolo8[2] = (Texture) Resources.Load("Icons/8/fundo");
		simbolo8[3] = (Texture) Resources.Load("Icons/8/fundoeborda");
		
		simbolo9[0] = (Texture) Resources.Load("Icons/9/semfundo");
		simbolo9[1] = (Texture) Resources.Load("Icons/9/borda");
		simbolo9[2] = (Texture) Resources.Load("Icons/9/fundo");
		simbolo9[3] = (Texture) Resources.Load("Icons/9/fundoeborda");

		ok = (Texture) Resources.Load("Icons/OK/fundo");
		okSeleccionado = (Texture) Resources.Load("Icons/OK/borda");

		numSelecionados = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.S)) {
			if ((indicadorY < 2) || (indicadorY < 3 && indicadorX == 2))
				indicadorY++;
		}
		if (Input.GetKeyDown(KeyCode.D)) {
			if (indicadorX < 2 && indicadorY != 3)
				indicadorX++;
		}
		if (Input.GetKeyDown(KeyCode.W)) {
			if (indicadorY > 0)
				indicadorY--;
		}
		if (Input.GetKeyDown(KeyCode.A)) {
			if (indicadorX > 0 && indicadorY != 3)
				indicadorX--;
		}
		if (Input.GetKeyDown(KeyCode.Return)) {
			if (indicadorX == 2 && indicadorY == 3) {
				if (selecao[0] && selecao[2] && selecao[8] && numSelecionados == 3)
				{
					//open door here
					GetComponent<DoorController>().doorAnimation.CrossFade("Abrir") ;
					GetComponent<DoorController>().specialDoor = false;
					GetComponent<DoorController>().isOpen = true;
					Time.timeScale = 1f;

					Destroy(this);
				}
				//clean seleccion
				for (int i = 0; i < 9; i++)
					selecao[i] = false;
				
				numSelecionados = 0;
			}
			else if (!selecao[indicadorY*3+indicadorX]) {
					numSelecionados++;
					selecao[indicadorY*3+indicadorX] = true;
			}
			else
			{
				numSelecionados--;
				selecao[indicadorY*3+indicadorX] = false;
			}
		}
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Time.timeScale = 1f;
			this.enabled = false;
		}
	
	}

	private Texture2D MakeTexture (Color color) {
		return MakeTexture (2, 2, color);
	}

	private Texture2D MakeTexture( int width, int height, Color color )
	{
		Color[] pix = new Color[width * height];
		for( int i = 0; i < pix.Length; ++i )
		{
			pix[ i ] = color;
		}
		Texture2D tex = new Texture2D( width, height );
		tex.SetPixels( pix );
		tex.Apply();
		return tex;
	}
	/*
	private Texture2D MakeTextureWithBorder( int width, int height, Color color, Color border )
	{
		Color[] pix = new Color[width * height];
		for( int i = 0; i < pix.Length; ++i )
		{
			if (( i < width || (i % width) == 0 || ((i+1) % width) == 0 || i / width == height-1 ) || (i < width*2 || (i-1) % width == 0 || (i+2) % width == 0 || i / width == height-2 ))
				pix [i] = border;
			else
				pix[ i ] = color;
		}
		Texture2D tex = new Texture2D( width, height );
		tex.SetPixels( pix );
		tex.Apply();
		return tex;
	}
	*/

	void OnGUI()
	{
		GUIStyle vazio = new GUIStyle (GUI.skin.box);
		vazio.normal.background = MakeTexture (Color.clear);
				
		int halfSizeIcon = sizeIcon / 2;
		int leftMargin = 10;
		int initialTopMargin =  100, topMargin = 10;

		if (indicadorY == 3 && indicadorX == 2) {
			GUI.Box (new Rect (Screen.width / 2 + halfSizeIcon + leftMargin, initialTopMargin + 3*sizeIcon + 3*topMargin, sizeIcon, sizeIcon), okSeleccionado, vazio);
		} else {			
			GUI.Box (new Rect (Screen.width / 2 + halfSizeIcon + leftMargin, initialTopMargin + 3*sizeIcon + 3*topMargin, sizeIcon, sizeIcon), ok, vazio);
		}

		for (int i = 0; i < 3; i++)
		{
			int heightMargin = initialTopMargin + i*sizeIcon + i*topMargin;

			Texture[] textures1 = simbolo1;
			Texture[] textures2 = simbolo1;
			Texture[] textures3 = simbolo1;

			if (i == 0)
			{
				textures1 = simbolo1;
				textures2 = simbolo2;
				textures3 = simbolo3;
			}
			else if (i == 1)
			{
				textures1 = simbolo4;
				textures2 = simbolo5;
				textures3 = simbolo6;
			}
			else if (i == 2)
			{
				textures1 = simbolo7;
				textures2 = simbolo8;
				textures3 = simbolo9;
			}

			Texture text1 = textures1[0];
			Texture text2 = textures2[0];
			Texture text3 = textures3[0];
			
			if (selecao[i*3])
				text1 = textures1[2];
			if (selecao[i*3+1])
				text2 = textures2[2];
			if (selecao[i*3+2])
				text3 = textures3[2];

			if (i == indicadorY)
			{
				if (indicadorX == 0)
				{
					if (selecao[i*3])
						text1 = textures1[3];
					else
						text1 = textures1[1];
				}

				else if (indicadorX == 1)
				{
					if (selecao[i*3+1])
						text2 = textures2[3];
					else
						text2 = textures2[1];
				}
				else if (indicadorX == 2)
				{
					if (selecao[i*3+2])
						text3 = textures3[3];
					else
						text3 = textures3[1];
				}
			}

			GUI.Box (new Rect (Screen.width / 2 - halfSizeIcon - sizeIcon - leftMargin, heightMargin, sizeIcon, sizeIcon), text1, vazio);
			GUI.Box (new Rect (Screen.width / 2 - halfSizeIcon, heightMargin, sizeIcon, sizeIcon), text2, vazio);
			GUI.Box (new Rect (Screen.width / 2 + halfSizeIcon + leftMargin, heightMargin, sizeIcon, sizeIcon), text3, vazio);
		}

	}
}
