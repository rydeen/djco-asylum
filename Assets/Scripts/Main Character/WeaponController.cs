using UnityEngine;
using System.Collections;
using System.Threading;



public class WeaponController : MonoBehaviour {


	public GameObject[] availableWeapons ; // available weapons 
	public int activeWeapon ; // index of 'availableWeapons' marking the active weapon

	public AudioClip soundChange;
	public AudioClip soundMenuChange;
	
	public GUISkin guiSkin;
	public GUIStyle styleBox;

	/* weapons menu on the top left */
	private int distanceXofmenu = 20;  
	private int distanceYofmenu = 20; 
	
	private int timeBetweenChangeweapons ;
	private bool menuWeapon;
	private Thread waitToChangeWeapon;
	private bool runActivateWeapon;
	private float timeStampChangeWeapond;
	private Animation armsAnimation;



	/**********************************************************************/
	/* *****************************  START ***************************** */
	
	void Start () 
	{	
		armsAnimation = GameObject.FindGameObjectWithTag ("Player_Arms").GetComponent<Animation>();
		timeStampChangeWeapond = 0;
		activateWeapon(false);
		this.menuWeapon = false;
		this.timeBetweenChangeweapons = 1000;	
	}


	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */

	void Update () {

		if (this.availableWeapons.Length == 0)
			this.activeWeapon = -1;
		
		if (Input.GetKeyDown (KeyCode.Q)) 
		{
			
			if(this.menuWeapon)
			{
				this.nextWeapon(true);			
			}
			else
			{
				this.menuWeapon = true;
				this.nextWeapon(false);
			}
		}
		
		if (this.runActivateWeapon) {
			if(!armsAnimation.IsPlaying("change_weapon"))
			{
			timeStampChangeWeapond = Time.time + 1;
			armsAnimation.CrossFade ("change_weapon");
			}
			else if( timeStampChangeWeapond < Time.time)
			{
			activateWeapon(true);
			}
		}

		if (Input.GetMouseButton (0)) {
			Debug.Log("Mouse pressed");
			availableWeapons [this.activeWeapon].gameObject.GetComponent<WeaponScript> ().setTryToAttack ();
		}


		
	}




	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */

	void OnGUI() 
	{
		if (this.activeWeapon < 0)
			return;

		if( this.menuWeapon )
		{
			// desenha arma Selecionada
			Texture t = this.availableWeapons[this.activeWeapon].gameObject.GetComponent<WeaponScript>().texture;
			GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu+50, 100, 55), ""); // Para dar o efeito que e a arma selecionada
			GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu+52, 100, 50), t);

			//Debug.Log("image width" + t.width +"  Image height " + t.height);


			//desenha arma "acima da selecionada"
			if((this.activeWeapon+1) < this.availableWeapons.Length)
			{
				t = this.availableWeapons[this.activeWeapon+1].gameObject.GetComponent<WeaponScript>().texture;
				GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu, 100, 50), t);
			}
			else if( this.availableWeapons.Length < 3)
			{
				GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu, 100, 50), ""); 

				//GUI.DrawTexture(new Rect( this.distanceXofmenu, this.distanceYofmenu, 100, 50), "");
			}
			else{
				t = this.availableWeapons[0].gameObject.GetComponent<WeaponScript>().texture;
				GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu,100,50),t);
			}


			// desenha arma " a baixo da selecionada"
			if((this.activeWeapon-1) >= 0)
			{
				t = this.availableWeapons[this.activeWeapon-1].gameObject.GetComponent<WeaponScript>().texture;
				GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu+105,100,50),t);
			}
			else if(this.availableWeapons.Length < 3)
			{
				GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu+105, 100, 50), ""); 
				//GUI.DrawTexture(new Rect( this.distanceXofmenu, this.distanceYofmenu+100, 100, 50), "");
			}
			else{
				t = this.availableWeapons[this.availableWeapons.Length-1].gameObject.GetComponent<WeaponScript>().texture;
				GUI.Box(new Rect( this.distanceXofmenu, this.distanceYofmenu+105,100,50),t);
			}

			GUI.skin = this.guiSkin;
			GUI.Label (new Rect ( this.distanceXofmenu+100, this.distanceYofmenu+52, 100 , 50 ), 
			           this.availableWeapons[this.activeWeapon].gameObject.GetComponent<WeaponScript>().weaponName);
		}
	}


	/**********************************************************************/
	/* ************************** NEXT  WEAPON ************************** */

	void nextWeapon(bool menuAlreadyOpen)
	{
		if (this.activeWeapon < 0)
			return;


		if (menuAlreadyOpen) 
		{
			waitToChangeWeapon.Abort ();

			this.activeWeapon = this.activeWeapon + 1 ;

			if (this.activeWeapon == this.availableWeapons.Length) // volta à primeira arma
				this.activeWeapon = 0 ;
		}

		this.GetComponent<AudioSource>().clip = soundMenuChange;
		this.GetComponent<AudioSource>().Play();		

		waitToChangeWeapon = new Thread(new ThreadStart(waitToChange));
		waitToChangeWeapon.Start ();
	}


	public void waitToChange()
	{
		Thread.Sleep(timeBetweenChangeweapons);
		this.menuWeapon = false;
		this.runActivateWeapon = true;

	}


	
	/**********************************************************************/
	/* ************************ ACTIVATE  WEAPON ************************ */

	void activateWeapon(bool playSound)
	{
		if (this.activeWeapon < 0)
			return;

		for (int i=0; i<this.availableWeapons.Length; i++) 
		{
			if ( i == this.activeWeapon )
				this.availableWeapons[i].GetComponent<MeshRenderer>().enabled = true ;//.SetActive(true) ;
			else
				this.availableWeapons[i].GetComponent<MeshRenderer>().enabled = false ;//.SetActive(false) ;
		}

		this.runActivateWeapon = false;

		if ( playSound )
		{
			this.GetComponent<AudioSource> ().clip = this.soundChange;
			this.GetComponent<AudioSource> ().Play ();	
		}
	}

	/**********************************************************************/
	/* ************************* ADD NEW WEAPON ************************* */

	public void addWeapon( GameObject weapon )
	{
		weapon.GetComponent<PickupWeapon> ().enabled = false;

		GameObject[] temp = new GameObject[this.availableWeapons.Length+1];
		
		for (int i=0; i<this.availableWeapons.Length; i++)
			temp [i] = this.availableWeapons [i];
		
		temp [temp.Length - 1] = weapon;
		this.availableWeapons = temp;		
	}

	/**********************************************************************/
	/* ************************* REMOVE  WEAPON ************************* */

	public void removeWeapon( GameObject weapon )
	{
		GameObject[] temp = new GameObject[this.availableWeapons.Length-1];
		int pos = -1; int index = 0;

		for ( int i=0 ; i<this.availableWeapons.Length ; i++ )
		{
			if( this.availableWeapons [i].gameObject.GetComponent<WeaponScript> ().weaponName ==
			   weapon.GetComponent<WeaponScript>().weaponName )
			{
				pos = i ;
				break ;
			}
		}

		for( int i=0 ; i<this.availableWeapons.Length ; i++ )
		{
			if ( i != pos )
			{
				Debug.Log(index) ;
				temp[index] = this.availableWeapons[i] ;
				index++ ;
			}
		}

		if (this.activeWeapon == pos && temp.Length>0) 
		{
			this.activeWeapon = 0 ;
		}
		else if ( temp.Length<=0 )
		{
			this.activeWeapon = -1 ;
		}

		this.availableWeapons = temp;
	}

}
