﻿using UnityEngine;
using System.Collections;
using System;

/*
 * ------------------------------------		FLASHLIGHT CONTROLLER	------------------------------------
 *
 *		this script is used to control the flashlight behaviour. 
 *
 *			> The player can store up to 7 batteries (6 in her pocket and 1 inside the flashlight)
 *			> The index 0 of the array 'batteries' is always the battery in use by the flashlight
 *
 *			> Below 'colorFlickeringLimit' of battery life, the flashlight will start to flicker (color changes)
 *				and below 'lightFlickeringLimit_MAX' it will start to turn on/off too
 *			> The light intensity with decreased over time, when the battery life is between 25% and 5%
 *				The lowest intensity value is set to be 0.3
 *
 *			> Controls
 *				F	::	toggle the flashlight On/Off
 *				R	::	reload the flashlight with another battery
 *				V	::	drop battery in use by the flashlight
 *
 * -----------------------------------------------------------------------------------------------------
 */	

public class FlashlightController : MonoBehaviour {


	private Light flashlight ; // flashlight object
	private bool lightOn ; // boolean value to control the on/off state

	public GameObject[] batteries ; // array of batteries in the character's pocket
	public BatteryScript battery ; // the battery in use by the flashlight
	private int MaxBatteriesCarry = 7 ; // number of batteries to store and carry in the pocket

	/* BATTERY ORDERING MODES */
	private int MD_LIFE_INC = 0 ;
	private int MD_LIFE_DESC = 1 ;
	private int MD_INUSE = 2 ;

	/* flickering mode variables */
	private bool isLightFlickering ;
	private bool isColorFlickering ;
	private bool reloadFlag ;

	private float colorFlickeringLimit ;
	private float lightFlickeringLimit_MAX ;
	private float lightFlickeringLimit_MIN ;
	private float decreaseFunction_M ;
	private float decreaseFunction_B ;

	public GUISkin guiSkin ;
	public string color ;
	private int rectWidth = 300 ;
	private int rectHeight = 40 ;

	/**********************************************************************/
	/* *****************************  START ***************************** */

	void Start () 
	{
		this.color = "#c8c8c8";

		this.colorFlickeringLimit = 8;
		this.lightFlickeringLimit_MAX = 4;
		this.lightFlickeringLimit_MIN = 1;
		// the intensity of the light will only be decreased until 0.3
		this.decreaseFunction_M = (float)(0.7 / (this.lightFlickeringLimit_MAX - this.lightFlickeringLimit_MIN));
		this.decreaseFunction_B = 1 - (this.lightFlickeringLimit_MAX * this.decreaseFunction_M);


		this.flashlight = this.gameObject.GetComponentInChildren<Light> ();

		if (this.batteries.Length > 0) {
			this.battery = this.batteries [0].GetComponent<BatteryScript> ();
			this.battery.batteryOn = true ;
		}
		else
			this.battery = null;

		if (this.battery != null) {
			if (this.battery.batteryLife == 0) 
				turnOff ();
			else 
				turnOn ();
		} else
			turnOff ();


		sortBatteries( MD_INUSE );


		/* flickering init */
		this.isColorFlickering = false;
		this.isLightFlickering = false;

		this.reloadFlag = false;
	}

	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */
	
	void Update () {

		if (Input.GetKeyDown (KeyCode.F)) // TURN ON/OFF
		{
			if ( this.lightOn )
				turnOff() ;
			else if ( this.battery != null )
				turnOn() ;
		}
		else if ( Input.GetKeyDown( KeyCode.R ) ) // RELOAD
		{
			reloadFlashlight() ;
			GameObject.FindGameObjectWithTag("Flashlight").SendMessage("updateNumBatteries");
		}
		else if ( Input.GetKeyDown ( KeyCode.V ) )
		{
			if ( dropFlashlightBattery() == 0 )
			{
				Debug.Log ("FlashlightController - [V] Battery Droped") ;
			}
			else
			{
				Debug.Log ("FlashlightController - [V] No battery to droped") ;
			}
		}



		// If we still have a battery inside our flashlight
		if (this.battery != null) 
		{
			// BATTERY LIFE DRAIN
			if (this.battery.batteryLife > 0 && this.lightOn) 
			{
				this.battery.batteryLife -= Time.deltaTime * this.battery.batteryLifeDrain;

				if ( this.battery.batteryLife <= this.colorFlickeringLimit ) // when we have less then X % of battery...
				{
					this.isColorFlickering = true ;
					this.flashlight.GetComponent<FlickeringLight>().ColorFlickering(true) ;

					if ( this.battery.batteryLife <= this.lightFlickeringLimit_MAX )
					{
						this.isLightFlickering = true ;
						this.flashlight.GetComponent<FlickeringLight>().LightFlickering (true) ;
						decreaseIntensity();
					}
				}
			}
	
			// if the battery ends, turn off the light
			if (this.battery.batteryLife <= 0 && this.lightOn) {
				turnOff ();
				this.isColorFlickering = false ;
				this.isLightFlickering = false ;
				removeBattery (0);
				this.battery = null;
				//Debug.Log ("FlashlightController - Battery discarded");

				// automatic reload
				//Debug.Log ("FlashlightController - Automatic reload");
				reloadFlashlight();

				// send message to update GUI
				GameObject.FindGameObjectWithTag("Flashlight").SendMessage("updateNumBatteries");
			}
		}
		else
		{
			if( this.batteries.Length > 0 ) // there are batteries to use
			{
				Debug.Log ("FlashlightController - [R] Reload your flashlight");
				this.reloadFlag = true ;
			}
		}

	}


	/**********************************************************************/
	/* ******************** DROP FLASHLIGHT  BATTERY ******************** */

	/*
	 * This method removes the battery inside the flashlight if existing.
	 * Returns 0 if the drop was successful.
	 * Return -1 if the flashlight has no battery to drop.
	 */
	int dropFlashlightBattery()
	{
		if (this.battery != null && this.batteries.Length>1) 
		{
			turnOff();
			this.battery.batteryOn = false ;
			removeBattery(0) ;

			return 0;
		}

		return -1;
	}

	/**********************************************************************/
	/* *********************** RELOAD  FLASHLIGHT *********************** */

	/*
	 * This method reloads the flashlight with a new battery if available.
	 * Returns 0 if the reload was successful.
	 * Returns -1 if there are no more batteries and the reload failed
	 */
	int reloadFlashlight() {

		// if there is no battery inside our flashlight...
		if ( this.battery == null )
		{
			// and if we have a battery on our 'pocket'...
			if ( this.batteries.Length > 0 )
			{
				turnOff ();
				Debug.Log ("FlashlightController - play reload animation here");

				// take it
				this.battery = this.batteries[0].GetComponent<BatteryScript> (); // and put it inside the flashlight
				this.battery.batteryOn = true ;

				this.isLightFlickering = false ;
				this.isColorFlickering = false ;
				turnOn () ;
				//Debug.Log("FlashlightController - Battery reload success");

				this.reloadFlag = false ;

				return 0;
			}
			else
			{
				//Debug.Log("FlashlightController - No more batteries. Reload failed");
				GameObject.FindGameObjectWithTag("Flashlight").SendMessage("NoMoreBatteries") ;
				return -1;
			}
		}
		else // if there is still an 'alive' battery inside the flashlight
		{
			if( this.batteries.Length > 1 ) // the first one is the one we are using
			{
				turnOff ();
				Debug.Log ("FlashlightController - play reload animation here");

				this.battery.batteryOn = false ; // the old one

				// drop battery on reload
				removeBattery(0) ;

				this.battery = this.batteries[0].GetComponent<BatteryScript> (); // change the battery (keeping the old one)
				this.battery.batteryOn = true ; // the new one

				sortBatteries( MD_INUSE );

				this.isColorFlickering = false ;
				this.isLightFlickering = false ;
				turnOn();
				//Debug.Log("FlashlightController - Battery reload success");

				// send message to update GUI
				GameObject.FindGameObjectWithTag("Flashlight").SendMessage("updateNumBatteries");

				this.reloadFlag = false ;

				return 0;
			}
			else
			{
				//Debug.Log("FlashlightController - No more batteries. Reload failed");
				GameObject.FindGameObjectWithTag("Flashlight").SendMessage("NoMoreBatteries") ;
				return -1;
			}
		}
	}

	/**********************************************************************/
	/* ************ ADD AND REMOVE BATTERIES FROM THE POCKET ************ */

	/*
	 *	This method adds a new object 'battery' to the 'batteries' array.
	 *	If the array is not full, this method will add the new element and returns 0.
	 *	If the array is full, this method will return -1.
	 */
	public int addBattery( GameObject battery ) {

		//if (this.batteries.Length == this.MaxBatteriesCarry)
		if ( isBatteriesPacketFull() )
			return -1;

		GameObject[] temp = new GameObject[this.batteries.Length+1];
		
		for (int i=0; i<this.batteries.Length; i++)
			temp [i] = this.batteries [i];
		
		temp [temp.Length - 1] = battery;
		this.batteries = temp;

		// send message to update GUI
		GameObject.FindGameObjectWithTag("Flashlight").SendMessage("updateNumBatteries");

		return 0;
	}

	/*
	 * This method removes the object in the 'batteryIndex' position of the array 'batteries'
	 */
	void removeBattery( int batteryIndex ) {
		
		GameObject[] temp = new GameObject[this.batteries.Length-1];
		if (temp.Length < 1) 
		{
			this.batteries = temp;
			return ;
		}
		
		for (int i=0; i<this.batteries.Length; i++) 
		{
			if ( i < batteryIndex )
				temp[i] = this.batteries[i] ;
			else if ( i > batteryIndex )
				temp[i-1] = this.batteries[i] ;
		}
		
		DestroyObject (this.batteries [batteryIndex]);
		
		this.batteries = temp;

		// send message to update GUI
		GameObject.FindGameObjectWithTag("Flashlight").SendMessage("updateNumBatteries");
	}

	public bool isBatteriesPacketFull() {
		if (this.batteries.Length < this.MaxBatteriesCarry)
			return false;
		else
			return true;
	}

	/**********************************************************************/
	/* *********************** ORDERING BATTERIES *********************** */

	/* return 0 if succeded
	 * return -1 if the mode is not accepted
	 * 
	 * modes:
	 * 	MD_LIFE_INC : order by battery life. the lower first
	 * 	MD_LIFE_DESC : order by battery life. the higher first
	 * 	MD_INUSE : the first one is the one in use
	 */
	int sortBatteries( int mode ) 
	{
		if (mode == MD_LIFE_INC) {
			Array.Sort (this.batteries, CompareBatteriesLife_INC);
			return 0;
		} else if (mode == MD_LIFE_DESC) {
			Array.Sort (this.batteries, CompareBatteriesLife_DESC);
			return 0;
		} else if (mode == MD_INUSE) {
			Array.Sort (this.batteries, CompareBatteriesInUse);
			return 0;
		} else
			return -1;
	}
	
	private static int CompareBatteriesLife_INC( GameObject battery1 , GameObject battery2 )
	{
		float v_battery1 = battery1.GetComponent<BatteryScript> ().batteryLife;
		float v_battery2 = battery2.GetComponent<BatteryScript> ().batteryLife;
		
		if (v_battery1 > v_battery2)
			return 1;
		else if (v_battery1 < v_battery2)
			return -1;
		else
			return 0;
	}

	private static int CompareBatteriesLife_DESC( GameObject battery1 , GameObject battery2 )
	{
		float v_battery1 = battery1.GetComponent<BatteryScript> ().batteryLife;
		float v_battery2 = battery2.GetComponent<BatteryScript> ().batteryLife;
		
		if (v_battery1 > v_battery2)
			return -1;
		else if (v_battery1 < v_battery2)
			return 1;
		else
			return 0;
	}

	private static int CompareBatteriesInUse( GameObject battery1 , GameObject battery2 )
	{
		bool b1 = battery1.GetComponent<BatteryScript> ().batteryOn;
		bool b2 = battery2.GetComponent<BatteryScript> ().batteryOn;

		if (b1 && !b2) // b1 in use
			return -1;
		else if (b2 && !b1) // b2 in use
			return 1;
		else // both not in use :: only one can be in use, order by full charge
			return 0;
	}

	/**********************************************************************/
	/* ****************** TOGGLE ON  AND OFF FUNCTIONS ****************** */

	/*
	 * This method turns off the flashlight and hides it
	 */
	void turnOff() {
		Debug.Log ("FlashlightController - [F] hiding flashlight animation");
		this.flashlight.GetComponent<FlickeringLight> ().ColorFlickering (false) ;
		this.flashlight.GetComponent<FlickeringLight> ().LightFlickering (false) ;
		this.lightOn = false;
		this.GetComponent<MeshRenderer> ().enabled = false;
		this.flashlight.enabled = false;
	}

	/*
	 * This method turns on the flashlight and displays it
	 */
	void turnOn() {
		Debug.Log ("FlashlightController - [F] showing flashlight animation");
		this.GetComponent<MeshRenderer> ().enabled = true;

		this.lightOn = true;
		this.flashlight.enabled = true;

		if ( this.isLightFlickering )
			this.flashlight.GetComponent<FlickeringLight> ().LightFlickering (true) ;
		else
			this.flashlight.GetComponent<FlickeringLight> ().LightFlickering (false) ;

		if (this.isColorFlickering)
			this.flashlight.GetComponent<FlickeringLight> ().ColorFlickering (true) ;
		else
			this.flashlight.GetComponent<FlickeringLight> ().ColorFlickering (false) ;
	}

	/**********************************************************************/
	/* ******************** DECREASE LIGHT INTENSITY ******************** */

	/*
	 * This method decreases the flashlight intensity over time, when the battery life is between 25% and 5%
	 */
	void decreaseIntensity() 
	{
		if ( (this.battery.batteryLife <= this.lightFlickeringLimit_MAX)
		    &&
		    (this.battery.batteryLife > this.lightFlickeringLimit_MIN) )
		{
			this.flashlight.intensity = ((this.decreaseFunction_M * this.battery.batteryLife) + this.decreaseFunction_B);
		}
	}


	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */
	
	void OnGUI()
	{
		if ( this.reloadFlag )
		{
			GUI.skin = this.guiSkin;
			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), (Screen.height/4)*3.5f, this.rectWidth , this.rectHeight ), 
			           "PRESS <color="+this.color+">[R]</color> TO RELOAD");
		}
	}

}
