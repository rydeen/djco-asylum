﻿using UnityEngine;
using System.Collections;

public class LifeScript : MonoBehaviour {

	public int life;
	public float timeStamp;
	public int lifeToIncrease;
	public int timeToIncrease;
	private bool fading = false;
	private bool lowLifeFade = false;
	// Use this for initialization
	void Start () {
		life = 30;
		timeStamp = 0;
		lifeToIncrease = 2;
		timeToIncrease = 2;
		Cursor.visible = false;

	}

	void OnGUI()
	{
		//GUI.Box(new Rect( 50, 50, 100, 55),life.ToString()); // Para dar o efeito que e a arma selecionada
		

	}
	
	// Update is called once per frame
	void Update () {



		if (life <= 0 && !fading) {
			Camera.main.SendMessage ("fullFadeOut");
			fading = true;
			Debug.Log ("Game OVER");
		} else if (life < 20 && !lowLifeFade) {
			Camera.main.SendMessage ("smallFadeOut");
			lowLifeFade = true;
		} else if(life >= 20){
			Camera.main.SendMessage ("fadeIn");
			lowLifeFade = false;
		}


		if(timeStamp <= Time.time && life < 100) {
			life += lifeToIncrease;
			if(life > 100)
				life = 100;

			timeStamp = Time.time + timeToIncrease;
		}
	}

	public void decreaseLife(int damage)
	{
		life -= damage;
	}




}
