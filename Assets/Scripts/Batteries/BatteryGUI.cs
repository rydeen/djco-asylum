﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class BatteryGUI : MonoBehaviour {

	private int numberOfBatteries ; 
	private bool update ;
	private FlashlightController flashlight ;

	public GUISkin guiSkin ;
	public Texture texture ;
	public string color ;
	private string auxColor ;

	private int rectWidth = 100 ;
	private int rectHeight = 40 ;

	/**********************************************************************/
	/* *****************************  START ***************************** */

	void Start () 
	{
		this.flashlight = this.gameObject.GetComponent<FlashlightController> ();

		this.update = true;

		this.color = "#c8c8c8";
		this.auxColor = this.color;
	}


	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */

	void Update () 
	{
		if (this.update) 
		{
			this.numberOfBatteries = this.flashlight.batteries.Length ;

			if (this.flashlight.battery != null)
			{
				this.numberOfBatteries-=1;
			}

			this.update = false ;
		}

	}

	void updateNumBatteries()
	{
		this.update = true;
	}

	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */
	
	void OnGUI()
	{
		GUI.skin = this.guiSkin;

		GUI.Label (new Rect ((Screen.width / 8) * 7, (Screen.height / 6) * 5, this.rectWidth, this.rectHeight), 
		           new GUIContent (  "<color="+this.color+">" + " " + this.numberOfBatteries + "</color>"  , this.texture));
	}

	void NoMoreBatteries()
	{
		Thread showError = new Thread(new ThreadStart( OnGUIError ));
		showError.Start() ;
	}

	void OnGUIError()
	{
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
	}
}
