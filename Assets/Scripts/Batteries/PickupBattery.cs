﻿using UnityEngine;
using System.Collections;

public class PickupBattery : MonoBehaviour {


	// this is a flag to tell when the player can take pickup a battery
	private bool actionEnabled = false ;
	private bool isFull = false ;

	public GUISkin guiSkin ;
	public string color ;

	private int rectWidth = 300 ;
	private int rectHeight = 40 ;


	void Start()
	{
		this.color = "#c8c8c8";
	}

	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */

	void Update()
	{
		if ( this.actionEnabled )
		{
			if ( Input.GetKey ( KeyCode.E ) )
			{
				FlashlightController flashlightScript = GameObject.FindGameObjectWithTag("Flashlight").GetComponent<FlashlightController>() ;

				if ( ! flashlightScript.isBatteriesPacketFull() )
				{
					GameObject pocket = GameObject.FindGameObjectWithTag("Batteries Pocket") ;

					MeshRenderer[] mr = this.gameObject.GetComponentsInChildren<MeshRenderer>() ;
					for ( int i=0 ; i<mr.Length ; i++ )
					{
						mr[i].enabled = false ;
					}


					this.actionEnabled = false ; // disable the label on the screen
					this.gameObject.transform.parent = pocket.transform ; // battery is now a child of the pocket
					flashlightScript.addBattery ( this.gameObject ) ; // add the battery to the array of batteries (FlashlighController)
				
				
					// send message to update GUI
					GameObject.FindGameObjectWithTag("Flashlight").SendMessage("updateNumBatteries");
				}
				else // if the pocket is full
				{
					Debug.Log ("PickupBattery script: Your pocket is full!") ;
					this.isFull = true ;
					this.actionEnabled = false ;
				}

			}
		}
	}


	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */

	void OnGUI() 
	{
		if ( this.actionEnabled )
		{
			GUI.skin = this.guiSkin;
			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), 
			                      (Screen.height/4)*3, 
			                      this.rectWidth , this.rectHeight ), 
			           "PRESS <color="+this.color+">[E]</color> TO COLLECT");
		}

		if ( this.isFull )
		{
			GUI.skin = this.guiSkin;
			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), 
			                     (Screen.height/4)*3, 
			                     this.rectWidth , this.rectHeight ), 
			           "YOUR POCKET IS <color="+this.color+">FULL</color>");
		}
	}

	/**********************************************************************/
	/* **************************** TRIGGERS **************************** */

	void OnTriggerEnter( Collider collider ) // when entering the collidering area...
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = true;
		}
	}

	void OnTriggerExit( Collider collider ) // when leaving the colliding area...
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = false;
			this.isFull = false ;
		}
	}
}
