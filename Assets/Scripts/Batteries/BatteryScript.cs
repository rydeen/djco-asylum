﻿using UnityEngine;
using System.Collections;

public class BatteryScript : MonoBehaviour {


	/* Battery unit: seconds */

	public float batteryLife ; // this represents the remaining battery in the flashlight
	public float fullBatteryLife ; // this is the maximum value of battery 
	public float batteryLifeDrain ; // battery consumption over time


	public bool batteryOn ; // true if this battery is on the flashlight


	void Start()
	{
		//this.fullBatteryLife = this.batteryLife;
		//this.fullBatteryLife = 100; // 100 seconds
		//this.batteryLifeDrain = 0.2f; // -1% every 5 seconds 

		this.batteryOn = false;
	}

}