﻿using UnityEngine;
using System.Collections;

/*
 * ------------------------------------		FLICKERING LIGHT	------------------------------------
 *
 *		this script is used to control the flickering flashlight behaviour. 
 *
 *			> The light will be turned on and off several times, keeping their state 
 *				between 'minTimeInterval' and 'maxTimeInterval' seconds
 *
 * -------------------------------------------------------------------------------------------------
 */	

public class FlickeringLight : MonoBehaviour {

	// light object (our flashlight)
	private Light flickeringLight ;

	// flickering light times
	public float minTimeInterval = 0.05f;
	public float maxTimeInterval = 0.25f ;
	private float lightTimer ;

	// light color
	private Color originalColor ;

	// a flag to indicate when to flicker the light
	public bool enableColorFlickering ;
	public bool enableLightFlickering ;

	/**********************************************************************/
	/* *****************************  START ***************************** */

	void Start () 
	{
		this.flickeringLight = this.gameObject.GetComponent<Light> ();
		this.originalColor = this.flickeringLight.color;

		this.enableColorFlickering = false;
		this.enableLightFlickering = false;

		this.lightTimer = Random.Range (this.minTimeInterval, this.maxTimeInterval);
	}
	
	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */

	void Update () 
	{
		if (this.enableColorFlickering)
			this.flickeringLight.color = this.originalColor * evalColorFunc (); // flicker (color changes)

		if (this.enableLightFlickering) // flicker (on / off)
		{
			this.lightTimer -= Time.deltaTime ; // decrease the timer..

			if ( this.lightTimer <= 0 )
			{
				this.flickeringLight.enabled = !this.flickeringLight.enabled ; // turn on/off the light
				this.lightTimer = Random.Range( this.minTimeInterval, this.maxTimeInterval ) ; // reset the timer
			}

		}
	}

	/**********************************************************************/
	/* ************** COLOR FLICKERING EVALUATION FUNCTION ************** */

	float evalColorFunc()
	{
		float f = (float)Random.Range (0.2f, 2.0f);

		if (f > 1)
			return 1.0f;
		else 
			return f;
	}

	/**********************************************************************/
	/* *********************** ON/OFF  FLICKERING *********************** */

	public void ColorFlickering(bool enable) 
	{ 
		this.enableColorFlickering = enable; 
		if (!enable && this.flickeringLight!=null )
			this.flickeringLight.color = this.originalColor;
	}
	public void LightFlickering(bool enable) 
	{ 
		this.enableLightFlickering = enable; 
	}
}
