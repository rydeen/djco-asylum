﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class NeedleController : WeaponScript {

	public bool inHand ;

	public int numberOfNeedles ;
	public int maxNumberOfNeedles ;

	public GameObject projectile ;
	public GUISkin guiSkin ;
	public Texture labelTexture ;
	public string color ;
	private string auxColor ;
	private int rectWidth = 100 ;
	private int rectHeight = 40 ;
	private float timeStampAttack;
	private bool releaseNeedle;
	public Transform startPos ;

	private float timeStamp;
	private float coolDownPeriod;
	private float timeStampEnableRender;
	private bool enableRender;

	void Start()
	{
		this.relativePosition = new Vector3 ((float)-0.31, (float)-0.15, (float)0.9);
		this.relativeRotation = Quaternion.Euler (30, 320, 320);
		this.weaponName = "NEEDLE";
		this.timeStampAttack = -1;
		this.armsAnimation = GameObject.FindGameObjectsWithTag ("Player_Arms")[0].GetComponent<Animation>();
		this.releaseNeedle = false;
		this.color = "#c8c8c8";
		this.auxColor = this.color;
		this.timeStamp = 0;
		this.coolDownPeriod = 2.0f;
		this.attackAnimationName = "attack_needle";
		this.myLayerMask = LayerMask.GetMask ("Player");
		this.damage = 10;
		this.timeStampEnableRender = 0;
		this.enableRender = false;
	}


	void Update()
	{
		if (enableRender && timeStampEnableRender < Time.time) {
			GetComponent<Renderer>().enabled = true;
			enableRender = false;
		}


		if (releaseNeedle) {
			if(Time.time > timeStampAttack)
			{
				Ray ray = Camera.main.ScreenPointToRay ( new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0)) ;
				GameObject thrown = (GameObject)Instantiate (this.projectile) ;
				thrown.transform.position = this.startPos.transform.position ;
				Rigidbody r = thrown.GetComponent<Rigidbody> () ;
				r.velocity = ray.direction * 20 ;
				this.numberOfNeedles-- ;
				releaseNeedle = false;
				GetComponent<Renderer>().enabled = false;
				enableRender = true;
			}
		}

		if ( tryToAttack ) // left mouse button
		{
			if ( this.numberOfNeedles > 0 && timeStamp <= Time.time )
			{
				attack () ;
				timeStamp = Time.time + coolDownPeriod;
			}
			else if ( this.numberOfNeedles == 0 )
			{
				Thread showError = new Thread(new ThreadStart( OnGUIError ));
				showError.Start() ;
			}

			tryToAttack = false;
		}
	}


	public override void attack ()
	{
		if (!armsAnimation.IsPlaying (attackAnimationName)) 
		{
			armsAnimation.CrossFade (attackAnimationName);
			releaseNeedle = true;
			timeStampAttack = Time.time + 0.6666666f;
			timeStampEnableRender = Time.time + 1.375f;

		}
	}


	void OnGUIError()
	{
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
		Thread.Sleep (250);
		this.color = "#ff0000";
		Thread.Sleep (250);
		this.color = this.auxColor;
	}

	void OnGUI()
	{
		if (this.inHand) {
			GUI.skin = this.guiSkin;
		
			GUI.Label (new Rect ((Screen.width / 8) * 7, (Screen.height / 6) * 4.5f, this.rectWidth, this.rectHeight), 
		           new GUIContent ("<color=" + this.color + ">" + " " + this.numberOfNeedles + "</color>", this.labelTexture));
		}
	}

	public bool IncNeedle()
	{
		if (this.numberOfNeedles < this.maxNumberOfNeedles) 
		{
			this.numberOfNeedles += 1;
			return true ;
		}
		else
		{
			return false ;
		}
	}
	public void setNumNeedles( int n )
	{
		this.numberOfNeedles = n;
	}

	public override void adjustPosition()
	{
		this.gameObject.transform.localPosition = new Vector3((float)0.0,(float)0.259,(float)0.11);
		this.gameObject.transform.localRotation = Quaternion.Euler(344,263,347);
	}
}
