﻿using UnityEngine;
using System.Collections;

public class HammerController : WeaponScript {
	
	void Start()
	{
		this.relativePosition = new Vector3 ((float)-0.31, (float)-0.15, (float)0.9);
		this.relativeRotation = Quaternion.Euler (30, 320, 320);
		this.weaponName = "HAMMER";
		this.range = 4;
		this.armsAnimation = GameObject.FindGameObjectsWithTag ("Player_Arms")[0].GetComponent<Animation>();
		this.attackAnimationName = "attack_hammer";
		this.damage = 20;
		this.myLayerMask = LayerMask.GetMask ("Player");
	}


	public override void attack ()
	{

	}

	public override void adjustPosition()
	{
		this.gameObject.transform.localPosition = new Vector3((float)0.0,(float)0.259,(float)0.11);
		this.gameObject.transform.localRotation = Quaternion.Euler(344,263,347);
	}
}
