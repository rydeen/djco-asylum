﻿using UnityEngine;
using System.Collections;

public class PickupNeedles : MonoBehaviour {

	private bool actionEnabled ;

	private bool isFull ;

	/* Visual Feedback Variables */
	public GUISkin guiSkin ;
	public string color ;

	private int rectWidth = 300 ;
	private int rectHeight = 40 ;


	void Start()
	{
		this.color = "#c8c8c8";
		this.isFull = false;
	}
	
	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */
	
	void Update()
	{
		if ( this.actionEnabled )
		{
			if ( Input.GetKey ( KeyCode.E ) )
			{
				GameObject aux = GameObject.Find("Needle");
				if ( aux != null )
				{
					if ( ! aux.GetComponent<NeedleController>().IncNeedle() )
					{
						this.isFull = true ;
					}
					else
					{
						this.isFull = false ;
						Destroy(this.gameObject) ;
					}
				}
			}
		}
	}

	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */
	
	void OnGUI() 
	{
		GUI.skin = this.guiSkin;

		if (this.actionEnabled && this.isFull) 
		{
			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), (Screen.height/4)*3, 
			                     this.rectWidth , this.rectHeight ), "<color=#ff0000> YOUR POCKET IS FULL </color>");
		}
		else if ( this.actionEnabled )
		{
			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), (Screen.height/4)*3, 
			                     this.rectWidth , this.rectHeight ), "PRESS <color="+this.color+">[E]</color> TO COLLECT");
		}		
	}
	
	/**********************************************************************/
	/* **************************** TRIGGERS **************************** */
	
	void OnTriggerEnter( Collider collider ) // when entering the collidering area...
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = true;
		}
	}
	
	void OnTriggerExit( Collider collider ) // when leaving the colliding area...
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = false;
			this.isFull = false ;
		}
	}
}
