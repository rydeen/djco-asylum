﻿using UnityEngine;
using System.Collections;

public class PickupWeapon : MonoBehaviour {


	private bool actionEnabled ;

	/* Visual Feedback Variables */
	public GUISkin guiSkin ;
	public string color ;
	
	private int rectWidth = 300 ;
	private int rectHeight = 40 ;


	void Start()
	{
		this.color = "#c8c8c8";
	}

	/**********************************************************************/
	/* ***************************** UPDATE ***************************** */
	
	void Update()
	{
		if ( this.actionEnabled )
		{
			if ( Input.GetKey ( KeyCode.E ) )
			{
				GameObject weaponBag = GameObject.FindGameObjectWithTag("Weapons Bag");
				this.gameObject.GetComponent<MeshRenderer>().enabled = false ; //.SetActive(false);
				this.actionEnabled = false ;
				this.gameObject.transform.parent = weaponBag.transform ;
				weaponBag.GetComponent<WeaponController>().addWeapon( this.gameObject ) ;
				this.gameObject.GetComponent<WeaponScript>().adjustPosition();

				if ( this.gameObject.GetComponent<NeedleController>() != null )
				{
					this.gameObject.GetComponent<NeedleController>().inHand = true ;
				}

				this.enabled = false;
			}
		}
	}


	/**********************************************************************/
	/* ************************ VISUAL  FEEDBACK ************************ */

	void OnGUI() 
	{
		if ( this.actionEnabled )
		{
			GUI.skin = this.guiSkin;
			GUI.Label (new Rect ( Screen.width/2 - (this.rectWidth/2), 
			                     (Screen.height/4)*3, 
			                     this.rectWidth , this.rectHeight ), 
			           "PRESS <color="+this.color+">[E]</color> TO COLLECT");
		}		
	}
	
	/**********************************************************************/
	/* **************************** TRIGGERS **************************** */
	
	void OnTriggerEnter( Collider collider ) // when entering the collidering area...
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = true;
		}
	}
	
	void OnTriggerExit( Collider collider ) // when leaving the colliding area...
	{
		if (collider.gameObject.tag == "Player") 
		{
			this.actionEnabled = false;
		}
	}
}
