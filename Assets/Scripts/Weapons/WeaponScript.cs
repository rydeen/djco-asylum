using UnityEngine;
using System.Collections;

public abstract class WeaponScript : MonoBehaviour { 

	public LayerMask myLayerMask;
	public int damage;
	public Texture texture; // image to display on the screen
	public string weaponName; // identifies the weapon
	public int range;
	public Vector3 relativePosition ;
	public bool tryToAttack = false;

	public string attackAnimationName;
	public Animation armsAnimation;

	public Quaternion relativeRotation ;

	public void setTryToAttack()
	{
		tryToAttack = true;
	}

	void Update()
	{
		Vector3 forward = Camera.main.transform.TransformDirection(Vector3.forward) * range;
		//Debug.DrawRay(Camera.main.transform.position, forward, Color.red);
		
		if (tryToAttack) {

			if(!armsAnimation.IsPlaying(attackAnimationName))
			{
			armsAnimation.CrossFade (attackAnimationName);
			//Debug.Log ("Playing " + attackAnimationName);
			Ray ray = new Ray(Camera.main.transform.position, forward);
			RaycastHit hit;
			
			if (Physics.Raycast (ray,out hit, range,~myLayerMask)) {	
					Debug.Log ("Tag Hit " + hit.transform.tag);
				if(hit.transform.tag.Equals("Enemy"))
					atackEnemy(hit.transform.gameObject);
			}
			}

			tryToAttack = false;


		}
	}

	void atackEnemy(GameObject enemy)
	{
		enemy.GetComponent<FollowAndAtack> ().sufferAttack(damage);
		Debug.Log ("Player attacking Enemy");
	}

	public abstract void adjustPosition ();
	/*{
		this.gameObject.transform.localPosition = new Vector3((float) -0.78,(float)0.2,(float)-0.278);
		this.gameObject.transform.localRotation = Quaternion.Euler(310,117,105);
	}*/

	public abstract void attack();

}