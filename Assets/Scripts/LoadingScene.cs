﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI ;

public class LoadingScene : MonoBehaviour {

	public GUISkin guiSkin ;
	private bool labelFadeIn ;
	private float labelAlphaValue ;
	
	public MovieTexture movie ;
	public string levelName ;
	
	private bool isLoading ;

	private GameObject MainCharacter ;

	void Start () 
	{
		GetComponent<RawImage> ().texture = this.movie as MovieTexture;
		GetComponent<RawImage> ().enabled = false;
		this.labelAlphaValue = 0.99f;
		this.labelFadeIn = true;
		
		this.isLoading = false;
	}
	
	public void LoadScene()
	{
		this.movie.loop = true;
		this.movie.Play ();

		this.MainCharacter = GameObject.Find ("NEW_Character");
		Object.DontDestroyOnLoad (this.MainCharacter);
		
		StartCoroutine (DisplayLoadingScreen ());
	}
	
	IEnumerator DisplayLoadingScreen()
	{
		GetComponent<RawImage> ().enabled = true;
		this.MainCharacter.transform.position = new Vector3 (-122.27f, 1.48f, -91.16f);
		this.MainCharacter.transform.rotation = Quaternion.LookRotation (new Vector3 (0f,94.7743f,0f));

		this.isLoading = true;
		
		Camera.main.SendMessage ("fadeIn");
		AsyncOperation async = Application.LoadLevelAsync (this.levelName);
		
		while( !async.isDone )
		{
			yield return null;
		}
	}
	
	
	void OnGUI()
	{
		GUI.skin = this.guiSkin;
		
		if (this.isLoading) 
		{
			if (!this.labelFadeIn && this.labelAlphaValue < 1f && this.labelAlphaValue > 0f) 
			{
				GUI.color = new Color (255, 255, 255, this.labelAlphaValue);
				this.labelAlphaValue -= 0.5f * Time.deltaTime;
				GUI.Label (new Rect (0, 0, Screen.width, Screen.height), "LOADING");
				
				if (this.labelAlphaValue <= 0f) 
				{
					this.labelAlphaValue = 0.1f;
					this.labelFadeIn = true;
				}
			} 
			else if (this.labelFadeIn && this.labelAlphaValue < 1f && this.labelAlphaValue > 0f) 
			{
				GUI.color = new Color (255, 255, 255, this.labelAlphaValue);
				this.labelAlphaValue += 0.5f * Time.deltaTime;
				GUI.Label (new Rect (0, 0, Screen.width, Screen.height), "LOADING");
				
				if (this.labelAlphaValue > 1f) 
				{
					this.labelAlphaValue = 0.9f;
					this.labelFadeIn = false;
				}
			}
		}
	}

}
