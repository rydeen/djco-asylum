﻿using UnityEngine;
using System.Collections;

public class TriggerEnemyFollow : MonoBehaviour {

	public GameObject enemy;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player")
		{
			FollowAndAtack script = enemy.GetComponent<FollowAndAtack>();

			if(script.chasing == false)
				script.StartChasing();
			//else script.chasing = false;

		}
	}

}
