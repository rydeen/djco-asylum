﻿using UnityEngine;
using System.Collections;


public class FollowAndAtack : MonoBehaviour {

	public Transform playerTransform;
	public bool chasing;
	public int maxDistance;
	public int damage;
	public float coolDownPeriod;
	public float timeStamp;
	public float currentDistance;
	private Animation enemyAnimation;
	public float rotationSpeed;
	private string idle;
	private string walk;
	public int monsterType; // 0- gordo-Lento 1- gordo-rapido 2-alto-lento 3- alto-rapido 4-mae
	private int[] lifes = new int[5];
	private int life;
	private float velocity = 2; 

	// Use this for initialization
	void Start () {

		this.playerTransform = GameObject.Find ("NEW_Character").gameObject.transform;

		lifes [0] = 50;
		lifes [1] = 80;
		lifes [2] = 50;
		lifes [3] = 80;
		lifes [4] = 50;

		chasing = false;
		if (monsterType == 0 || monsterType ==1 || monsterType == 4) {
			maxDistance = 5;
			damage = 10;
			coolDownPeriod = 3;
			life = lifes[0];
			idle = "idle1";
		} 
		else if(monsterType == 2 || monsterType == 3) {
			maxDistance = 7;
			damage = 20;
			coolDownPeriod = 5;
			life= lifes[1];
			idle = "idle2";

		}



		if (monsterType == 1 || monsterType == 3) {
			walk = "run";
			velocity = 4;
		} else if (monsterType == 0 || monsterType == 2|| monsterType == 4) {
			walk = "walk";
			velocity = 2;
		}


		timeStamp = 0;
		//playerTransform = GameObject.FindGameObjectWithTag("Player").gameObject.transform;
		enemyAnimation = GetComponent<Animation>();
		rotationSpeed = 10f;
		GetComponent<NavMeshAgent> ().speed = velocity;



	}

	public void StartChasing () {
		chasing = true;

	}
	
	// Update is called once per frame
	void Update () 
	{

		if (!enemyAnimation.IsPlaying ("dead")) {

			// se esta parado
			if (GetComponent<NavMeshAgent> ().remainingDistance == 0) {
				if (!enemyAnimation.IsPlaying (idle))
				if (enemyAnimation.IsPlaying ("attack"))// || enemyAnimation.IsPlaying ("attacked"))
					enemyAnimation.CrossFadeQueued (idle);
				else
					enemyAnimation.CrossFade (idle);
			} else {
				if (!enemyAnimation.IsPlaying (walk))
				if (enemyAnimation.IsPlaying ("attack") )//|| enemyAnimation.IsPlaying ("attacked"))
						enemyAnimation.CrossFadeQueued (walk);
				else
					enemyAnimation.CrossFade (walk);
			}

			//Debug.Log ("currentDistance: "+currentDistance);

			currentDistance = Vector3.Distance (transform.position, playerTransform.position);

			if (chasing) {
				if (currentDistance > maxDistance)
					GetComponent<NavMeshAgent> ().destination = playerTransform.position;
				else {
					RotateTowards ();
					GetComponent<NavMeshAgent> ().destination = transform.position;
				}
			}

			if (currentDistance < maxDistance && timeStamp <= Time.time && monsterType !=4) {
				attack ();
				timeStamp = Time.time + coolDownPeriod;
			}

		}
	}


	private void RotateTowards () {
		Vector3 direction = (playerTransform.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(direction);
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
	}

	public void sufferAttack(int dmg)
	{
		if (!enemyAnimation.IsPlaying ("dead") ) {
			if(monsterType !=4)
			{
			//Debug.Log ("INImIGO levou dano, vida atual: " + (life - dmg));
			life -= dmg;
			
			if (life <= 0) {
				life = lifes[monsterType];
				enemyAnimation.CrossFade ("dead");
					//Debug.Log ("INImIGO a ZERO");
			} //else enemyAnimation.CrossFade ("attacked");
			}
		}


	}

	void attack()
	{
		/*if(enemyAnimation.IsPlaying("attacked"))
		enemyAnimation.CrossFadeQueued("attack");
		else*/ enemyAnimation.CrossFade("attack");

		playerTransform.GetComponent<LifeScript> ().decreaseLife (damage);
		//Debug.Log ("attacking");
	}
}
